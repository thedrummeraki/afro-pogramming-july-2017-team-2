$(document).ready(function() {
    var isAdvancedUpload = function() {
      var div = document.createElement('div');
      return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
    }();

    var $form = $('.mybox');
    var $submit_btn = $('#submit_btn');
    var $errorMsg = $('#error_msg');
    var $res_message = $('#res_message');
    var $input = $form.find('input[type="file"]'),
    $label = $form.find('label'),
    showFiles = function(files) {
        $label.text(files.length > 1 ? ($input.attr('data-multiple-caption') || '').replace( '{count}', files.length ) : files[ 0 ].name);
    };

    if (isAdvancedUpload) {
        $form.addClass('has-advanced-upload');
        var droppedFiles = false;

        $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
            e.preventDefault();
            e.stopPropagation();
        }).on('dragover dragenter', function() {
            $form.addClass('is-dragover');
        }).on('dragleave dragend drop', function() {
            $form.removeClass('is-dragover');
        }).on('drop', function(e) {
            droppedFiles = e.originalEvent.dataTransfer.files;
            showFiles( droppedFiles );
        });
    }

    $input.on('change', function(e) {
        showFiles(e.target.files);
        $submit_btn.removeClass('disabled');
    });

    $submit_btn.on('click', function(e) {
        document.getElementById("result").setAttribute('src', '');
        $res_message.text('');
        $form.removeClass('is-uploading');
        $form.removeClass('is-error');
        $form.removeClass('is-success');
    });

    $form.on('submit', function(e) {
        if ($form.hasClass('is-uploading')) return false;

        $form.addClass('is-uploading').removeClass('is-error');

        if (isAdvancedUpload) {
            e.preventDefault();

            var ajaxData = new FormData($form.get(0));

            if (droppedFiles) {
                $.each( droppedFiles, function(i, file) {
                    ajaxData.append( $input.attr('name'), file );
                });
            }

            $.ajax({
                url: $form.attr('action'),
                type: $form.attr('method'),
                data: ajaxData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                complete: function() {
                    $form.removeClass('is-uploading');
                },
                success: function(data) {
                    $form.removeClass('is-uploading');
                    $form.addClass(data.success == true ? 'is-success' : 'is-error');
                    if (!data.success) $errorMsg.text(data.error || "We're sorry but we counld't process this image.");
                    if (data.result) {
                        console.log("Received %d bytes of data", data.result.length);
                        data.result = data.result.replace(/[\r\n]/g, '');
                        console.log("Working with %d bytes of data", data.result.length);
                        var b64image = 'data:image/png;base64,' + data.result;
                        document.getElementById("result").setAttribute('src', b64image);
                    }
                    if (data.message) $res_message.text(data.message);
                    $submit_btn.text("Re-upload");
                },
                error: function() {
                    // Log the error, show an alert, whatever works for you
                }
            });
        } else {
            var iframeName  = 'uploadiframe' + new Date().getTime();
            $iframe   = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');

            $('body').append($iframe);
            $form.attr('target', iframeName);

            $iframe.one('load', function() {
                var data = JSON.parse($iframe.contents().find('body' ).text());
                $form
                  .removeClass('is-uploading')
                  .addClass(data.success == true ? 'is-success' : 'is-error')
                  .removeAttr('target');
                if (!data.success) $errorMsg.text(data.error);
                $form.removeAttr('target');
                $iframe.remove();
            });
        }
    });
});
