var express = require('express');
var app = express();
var fs = require('fs');
var request = require("request");
var bodyParser = require("body-parser");
var path = require("path");
var canvas = require("canvas");
var upload = require("multer")({dest: 'uploads/'});

// Middleware
app.use(bodyParser.json({uploadDir:'/tmp/.emojifacer/images'}));
app.use(express.static('public'));

// Google Cloud Client
var Vision = require('@google-cloud/vision');
var vision = Vision();

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

function highlightFaces (inputFile, faces, outputFile, Canvas, callback) {
  fs.readFile(inputFile, function (err, image) {
    if (err) {
      return callback(null, err);
    }

    if (faces.length <= 0) {
      return callback(inputFile, null, true);
    }

    var Image = Canvas.Image;
    // Open the original image into a canvas
    var img = new Image();
    img.src = image;
    var canvas = new Canvas(img.width, img.height);
    var context = canvas.getContext('2d');
    context.drawImage(img, 0, 0, img.width, img.height);

    // Now draw boxes around all the faces
    context.strokeStyle = 'rgba(0,255,0,0.8)';
    context.lineWidth = '5';

    faces.forEach(function (face) {
      context.beginPath();
      face.bounds.face.forEach(function (bounds) {
        context.lineTo(bounds.x, bounds.y);
      });
      context.lineTo(face.bounds.face[0].x, face.bounds.face[0].y);
      context.stroke();
    });

    // Write the result to a file
    console.log('Writing to file ' + outputFile);
    var writeStream = fs.createWriteStream(outputFile);
    var pngStream = canvas.pngStream();

    pngStream.on('data', function (chunk) {
      writeStream.write(chunk);
    });
    pngStream.on('error', console.log);
    pngStream.on('end', callback);
    callback(outputFile);
  });
}

function detectFaces(fileName, callback) {
    var result = {};
    vision.detectFaces(fileName)
    .then((results) => {
        const faces = results[0];

        console.log('Faces:');
        result.faces = [];
        faces.forEach((face, i) => {
            console.log(`  Face #${i + 1}:`);
            var our_face = face;
            
            our_face.id = i;
            var dimensions = {
                width: face.bounds.face[1].x - face.bounds.face[0].x,
                height: face.bounds.face[2].y - face.bounds.face[1].y
            };
            our_face.dimensions = dimensions;

            var nose = face.features.nose;

            our_face.location = {
                center: {x: nose.top.x, y: nose.top.y},
                top: {  
                    left: {x: face.bounds.face[0].x, y: face.bounds.face[0].y},
                    right: {x: face.bounds.face[1].x, y: face.bounds.face[1].y},
                },
                bottom: {
                    left: {x: face.bounds.face[2].x, y: face.bounds.face[2].y},
                    right: {x: face.bounds.face[3].x, y: face.bounds.face[3].y},
                }
            };

            our_face.expressions = [];
            var expressions = ["joy", "sorrow", 'anger', 'surprise'];
            [].forEach.call(expressions, function(exp) {
                if (face[exp]) {
                    our_face.expressions.push(exp);
                }
            });

            result.faces.push(our_face);
        });
        callback(true, result);
    }).catch((err) => {
        callback(false, {err: err});
        console.error('ERROR:', err);
    });
}

app.post('/file/upload', upload.any(), function(req, res) {
    var success = true;
    if (req.files && req.files.length) {
        var file = req.files[0];
        if (file.mimetype.indexOf('image') >= 0) {
            var fileName = __dirname + '/uploads/' + file.filename;
            var outFileName = __dirname + '/res/' + file.filename;
            detectFaces(fileName, function(success, result) {
                if (!success) {
                    res.send({success: false, result: result});
                } else {
                    highlightFaces(fileName, result.faces, outFileName, canvas, function(out, err, no_faces) {
                        console.log("Done: " + success);
                        fs.readFile(out, function (err, image) {
                            console.log(image);
                            image = new Buffer(image).toString('base64');
                            var message = no_faces ? "We counld't find any faces, sorry :(" : "Yay, we found " + result.faces.length + " face(s)!";
                            res.send({success: success, result: image, message: message});
                        });
                    });
                }
            });
        } else {
            console.log("Please upload a valid image (using %s)", file.mimetype);
            res.send({success: false, error: "Please upload a valid image."});
        }
    } else {
        console.log("No files were uploaded.");
        res.send({success: false, error: "No files were uploaded."});
    }
});

var port = process.env.PORT || 3000;
app.listen(port, function() {
    console.log("Running on %s!", port);
});
